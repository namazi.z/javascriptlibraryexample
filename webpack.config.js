const path = require('path')
const HtmlWebPack = require('html-webpack-plugin')

module.exports = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'bundle.js',
    library: 'bundle',
    libraryTarget: 'umd',
    globalObject: 'this'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPack({
      title: 'Web Application By Webpack',
      filename: path.join(__dirname, 'public/examples/browser/index.html')
    })
  ]
  //   devServer: {
  //     contentBase: path.join(__dirname, 'public/examples/browser/index.html'),
  //     port: 9090,
  //     publicPath: '/dist/'
  //   }
}
